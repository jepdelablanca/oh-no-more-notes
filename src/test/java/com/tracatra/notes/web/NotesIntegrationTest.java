package com.tracatra.notes.web;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tracatra.notes.dao.NoteDao;
import com.tracatra.notes.note.Note;
import com.tracatra.notes.note.NoteService;

@AutoConfigureMockMvc
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@WithMockUser(username = "admin", password = "admin", authorities = { "ROLE_ADMIN", "ROLE_USER" })
class NotesIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private NoteService noteService;

	@Autowired
	private NoteDao noteDao;

	@Autowired
	private ObjectMapper mapper;

	@Test
	public void testGiven2Notes_whenListNotes_then2NotesFound() throws Exception {
		noteService.addNote(noteOf("admin", "TEST-LIST", "testing list notes"));
		noteService.addNote(noteOf("admin", "TEST-LIST", "testing list notes 2"));

		MvcResult result = mockMvc.perform(get("/note").secure(true)).andExpect(status().isOk()).andReturn();

		List<Note> notes = Arrays.asList((Note[]) asObject(result.getResponse().getContentAsString(), Note[].class));
		assertTrue(notes.stream().filter(note -> "TEST-LIST".equals(note.getTitle())).count() == 2,
				"There should be 2 TEST-LIST notes");
	}

	@Test
	public void testWhenAddNote_thenNoteExists() throws Exception {
		mockMvc.perform(post("/note").secure(true).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(asJson(createNoteOf("TEST-ADD", "testing adding notes")))).andExpect(status().isCreated());

		assertTrue(noteDao.getUserNotes("admin").stream().anyMatch(note -> "TEST-ADD".equals(note.getTitle())),
				"Note TEST-ADD should be added");
	}

	@Test
	public void testGivenANote_whenNoteEdited_thenNoteIsUpdated() throws Exception {
		Long noteId = noteDao.create(noteOf("admin", "TEST-EDIT", "Text before edition")).getId();

		mockMvc.perform(put("/note").secure(true).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(asJson(updateNoteOf(noteId, "TEST-EDIT", "Text after edition")))).andExpect(status().isOk());

		Note note = noteDao.getNote(noteId).get();
		assertTrue("TEST-EDIT".equals(note.getTitle()) && "Text after edition".equals(note.getText()),
				"Note text should be modified");
	}

	@Test
	public void testGivenANote_whenRemoveNote_thenNoteDissapears() throws Exception {
		Long noteId = noteDao.create(noteOf("admin", "TEST-DELETE", "Something")).getId();

		mockMvc.perform(delete("/note/" + noteId).secure(true)).andExpect(status().isOk());

		assertTrue(noteDao.getNote(noteId).isEmpty(), "Note should be deleted");
	}

	@Test
	public void testGivenNotes_whenSearchNotes_thenNotesFound() throws Exception {
		noteService.addNote(noteOf("test", "TEST-SEARCH", "TEXT A"));
		noteService.addNote(noteOf("test", "TEST-SEARCH", "TEXT B"));
		noteService.addNote(noteOf("admin", "TEST-SEARCH", "TEXT C"));

		MvcResult result = mockMvc.perform(put("/note/query").secure(true).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(asJson(queryOf("TEST-SEARCH", null, null, 1)))).andExpect(status().isOk()).andReturn();
		NotesDto notesList = (NotesDto) asObject(result.getResponse().getContentAsString(), NotesDto.class);
		assertTrue(notesList.getCurrentPage() == 0 && notesList.getTotalItems() == 3
				&& "TEST-SEARCH".equals(notesList.getData().get(0).getTitle()), "3 notes should be found");

		MvcResult result2 = mockMvc.perform(put("/note/query").secure(true)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(asJson(queryOf(null, "TEXT B", null, 2))))
				.andExpect(status().isOk()).andReturn();
		NotesDto notesList2 = (NotesDto) asObject(result2.getResponse().getContentAsString(), NotesDto.class);
		assertTrue(notesList2.getCurrentPage() == 0 && notesList2.getTotalItems() == 1
				&& "TEXT B".equals(notesList2.getData().get(0).getText()), "Note should be found");

		MvcResult result3 = mockMvc.perform(put("/note/query").secure(true)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(asJson(queryOf(null, null, "test", 3))))
				.andExpect(status().isOk()).andReturn();
		NotesDto notesList3 = (NotesDto) asObject(result3.getResponse().getContentAsString(), NotesDto.class);
		assertTrue(notesList3.getCurrentPage() == 0 && notesList3.getTotalItems() == 2, "2 notes should be found");
	}

	private Note noteOf(String user, String title, String text) {
		return new Note(null, user, title, text, null, null, null);
	}

	private CreateNoteDto createNoteOf(String title, String text) {
		return CreateNoteDto.of(title, text);
	}

	private UpdateNoteDto updateNoteOf(Long id, String title, String text) {
		return UpdateNoteDto.of(id, title, text);
	}

	private QueryDto queryOf(String title, String text, String user, int sortMode) {
		switch (sortMode) {
		case 1:
			return QueryDto.of(user, title, text, 0, 10, "title", 1);
		case 2:
			return QueryDto.of(user, title, text, 0, 10, "text", -1);
		default:
			return QueryDto.of(user, title, text, 0, 10, null, null);
		}
	}

	private String asJson(Object object) throws Exception {
		return mapper.writeValueAsString(object);
	}

	private <T> Object asObject(String json, Class<T> objectClass) throws Exception {
		return mapper.readValue(json, objectClass);
	}
}
