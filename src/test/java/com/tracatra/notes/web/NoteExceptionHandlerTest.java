package com.tracatra.notes.web;

import java.util.Date;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.tracatra.notes.note.Note;
import com.tracatra.notes.note.NoteException;
import com.tracatra.notes.note.NoteQuery;
import com.tracatra.notes.note.NoteService;

@WebMvcTest(controllers = NoteController.class)
@WithMockUser(username = "admin", password = "admin", authorities = { "ROLE_ADMIN", "ROLE_USER" })
class NoteExceptionHandlerTest {

	private static final String NEW_NOTE = "{\"title\":\"TEST-ADD\"}";
	private static final String EXISTING_NOTE = "{\"id\":1,\"title\":\"TEST-ADD\"}";
	private static final String SEARCH_QUERY = "{\"from\":0,\"pageSize\":10}";

	private static final RequestPostProcessor AUTH_POSTPROCESSOR = 
			SecurityMockMvcRequestPostProcessors.httpBasic("admin", "admin");
	private static final RequestPostProcessor INVALID_AUTH_POSTPROCESSOR = 
			SecurityMockMvcRequestPostProcessors.httpBasic("invalid_user", "invalid_password");

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private Translator translator;

	@MockBean
	private NoteService notesService;

	@ParameterizedTest
	@MethodSource("noteRequestArgumentProvider")
	@DisplayName("Checking service failure handling")
	public void whenNoteServiceFails_thenAllMethodsReturn500Error(HttpMethod method, String path, String body, String stubbedMethod, Object[] stubbedMethodArguments) throws Exception {
		
		ReflectionTestUtils.invokeMethod(Mockito.doThrow(NoteException.class).when(notesService), stubbedMethod, stubbedMethodArguments);
		
		mockMvc.perform(prepareBuilder(method, path, body, AUTH_POSTPROCESSOR)).andExpect(MockMvcResultMatchers.status().is5xxServerError());
	}

	@ParameterizedTest
	@MethodSource("noteRequestArgumentProvider")
	@DisplayName("Checking unauthorized failure handling")
	public void whenInvalidUser_thenAllMethodsReturn401Error(HttpMethod method, String path, String body) throws Exception {
		
		mockMvc.perform(prepareBuilder(method, path, body, INVALID_AUTH_POSTPROCESSOR)).andExpect(MockMvcResultMatchers.status().isUnauthorized());
	}

	@ParameterizedTest
	@MethodSource("noteRequestArgumentProvider")
	@DisplayName("Checking unknown failure handling")
	public void whenRuntimeException_thenReturn500Error(HttpMethod method, String path, String body, String stubbedMethod, Object[] stubbedMethodArguments) throws Exception {
		
		ReflectionTestUtils.invokeMethod(Mockito.doThrow(RuntimeException.class).when(notesService), stubbedMethod, stubbedMethodArguments);
		
		mockMvc.perform(prepareBuilder(method, path, body, AUTH_POSTPROCESSOR)).andExpect(MockMvcResultMatchers.status().is5xxServerError());
	}

	private static Stream<Arguments> noteRequestArgumentProvider() throws Exception {
		return Stream.of(
				Arguments.of(HttpMethod.POST,	"/note",		NEW_NOTE,		"addNote",			new Object[] {createNewNote()}),
				Arguments.of(HttpMethod.PUT,	"/note",		EXISTING_NOTE,	"updateNote",		new Object[] {createExistingNote()}),
				Arguments.of(HttpMethod.DELETE,	"/note/1",		null,			"deleteNote",		new Object[] {"admin", 1l}),
				Arguments.of(HttpMethod.GET,	"/note",		null,			"listUserNotes",	new Object[] {"admin"}),
				Arguments.of(HttpMethod.PUT,	"/note/query",	SEARCH_QUERY,	"findNotes",		new Object[] {createNoteQuery()})
		);
	}

	private MockHttpServletRequestBuilder prepareBuilder(HttpMethod method, String path, String body, RequestPostProcessor auth) {
		MockHttpServletRequestBuilder result = 
			MockMvcRequestBuilders.request(method, path)
				.secure(true)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(auth);
		if(body!=null) {
			result
			.contentType(MediaType.APPLICATION_JSON_VALUE)
			.content(body);
		}
		return result;
	}

	private static Note createNewNote() {
		return new Note(null, "admin", "Shopping list", "eggs, tomatoes, onions", null, null, null);
	}

	private static Note createExistingNote() {
		return new Note(1l, "admin", "Shopping list", "eggs, tomatoes, onions", "CREATED", new Date(), new Date());
	}

	private static NoteQuery createNoteQuery() {
		Note sample = new Note(null, null, "list", null, null, null, null);
		return new NoteQuery(sample, null, null, null, null);
	}
}
