package com.tracatra.notes.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tracatra.notes.note.Note;
import com.tracatra.notes.note.Notes;
import com.tracatra.notes.note.NoteQuery;
import com.tracatra.notes.note.NoteService;

@WebMvcTest(controllers = NoteController.class)
@WithMockUser(username = "admin", password = "admin", authorities = { "ROLE_ADMIN", "ROLE_USER" })
class NoteControllerTest {

	private static final String NEW_NOTE_CONTENT = "{\"title\":\"TEST-ADD\"}";
	private static final String EXISTING_NOTE_CONTENT = "{\"id\":1,\"title\":\"TEST-ADD\"}";
	private static final String SEARCH_QUERY_CONTENT = "{\"from\":0,\"pageSize\":10}";

	private static final RequestPostProcessor AUTH_POSTPROCESSOR = 
			SecurityMockMvcRequestPostProcessors.httpBasic("admin", "admin");

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private Translator translator;

	@MockBean
	private NoteService notesService;

	@Autowired
	private ObjectMapper mapper;

	@Test
	@DisplayName("Checking successful note creation")
	public void whenCreateNote_thenNoteCreated() throws Exception {
		Note note = createExistingNote();
		Mockito.when(notesService.addNote(Mockito.any(Note.class))).thenReturn(note);
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/note")
				.secure(true)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(NEW_NOTE_CONTENT)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			).andExpect(MockMvcResultMatchers.status().isCreated());
	}

	@Test
	@DisplayName("Checking invalid create request failure handling")
	public void whenCreateInvalidNote_thenReturnsInvalidRequest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.post("/note")
				.secure(true)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{}")
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			).andExpect(MockMvcResultMatchers.status().is4xxClientError());
	}

	@Test
	@DisplayName("Checking successful note update")
	public void whenUpdateNote_thenNoteUpdated() throws Exception {
		Note note = createExistingNote();
		Mockito.when(notesService.updateNote(note)).thenReturn(note);
		
		mockMvc.perform(MockMvcRequestBuilders
				.put("/note")
				.secure(true)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(EXISTING_NOTE_CONTENT)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@DisplayName("Checking invalid update request failure handling")
	public void whenUpdateInvalidNote_thenReturnsInvalidRequest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.put("/note")
				.secure(true)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{}")
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			).andExpect(MockMvcResultMatchers.status().is4xxClientError());
	}

	@Test
	@DisplayName("Checking successful note deletion")
	public void givenANote_whenDeleteNote_thenNoteDeleted() throws Exception {
		Note note = createExistingNote();
		Mockito.when(notesService.deleteNote("admin", 1l)).thenReturn(note);
		
		mockMvc.perform(MockMvcRequestBuilders
				.delete("/note/1")
				.secure(true)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@DisplayName("Checking invalid delete request failure handling")
	public void whenDeleteInvalidNote_thenReturnsInvalidRequest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.delete("/note/this_is_not_a_number")
				.secure(true)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			).andExpect(MockMvcResultMatchers.status().is4xxClientError());
	}

	@Test
	@DisplayName("Checking succesful empty note listing")
	public void givenNoNotes_whenListingNotes_thenNoNotesFound() throws Exception {
		Mockito.when(notesService.listUserNotes(Mockito.anyString())).thenReturn(new ArrayList<>());
		
		String responseBody = mockMvc.perform(MockMvcRequestBuilders
				.get("/note")
				.secure(true)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			)
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andReturn().getResponse().getContentAsString();
		
		Assertions.assertEquals("[]", responseBody);
	}

	@Test
	@DisplayName("Checking succesful note listing")
	public void givenANote_whenListingNotes_thenNoteFound() throws Exception {
		Note resultNote = Mockito.mock(Note.class);
		Mockito.when(resultNote.getId()).thenReturn(1l);
		Mockito.when(notesService.listUserNotes(Mockito.anyString())).thenReturn(List.of(resultNote));
		
		String responseBody = mockMvc.perform(MockMvcRequestBuilders
				.get("/note")
				.secure(true)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			)
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andReturn().getResponse().getContentAsString();
		List<Note> response = mapper.readValue(responseBody, new TypeReference<List<Note>>() {});
		
		Assertions.assertNotNull(response);
		Assertions.assertEquals(1, response.size());
		Assertions.assertEquals(1l, response.get(0).getId());
	}

	@Test
	@DisplayName("Checking successful empty note search")
	public void givenNoNotes_whenSearchingNotes_thenNoNoteFound() throws Exception {
		Mockito.when(notesService.findNotes(Mockito.any(NoteQuery.class))).thenReturn(new Notes(List.of(), 0, 0));
		
		String responseBody = mockMvc.perform(MockMvcRequestBuilders
				.put("/note/query")
				.secure(true)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(SEARCH_QUERY_CONTENT)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			)
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andReturn().getResponse().getContentAsString();
		NotesDto response = mapper.readValue(responseBody, NotesDto.class);
		
		Assertions.assertNotNull(response);
		Assertions.assertEquals(0, response.getCurrentPage());
		Assertions.assertEquals(0, response.getTotalItems());
		Assertions.assertNotNull(response.getData());
		Assertions.assertEquals(0, response.getData().size());
	}

	@Test
	@DisplayName("Checking successful note search")
	public void givenANote_whenSearchingNotes_thenNoteFound() throws Exception {
		Note resultNote = Mockito.mock(Note.class);
		Mockito.when(resultNote.getId()).thenReturn(1l);
		Mockito.when(notesService.findNotes(Mockito.any(NoteQuery.class))).thenReturn(new Notes(List.of(resultNote), 0, 1));
		
		String responseBody = mockMvc.perform(MockMvcRequestBuilders
				.put("/note/query")
				.secure(true)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(SEARCH_QUERY_CONTENT)
				.with(SecurityMockMvcRequestPostProcessors.csrf())
				.with(AUTH_POSTPROCESSOR)
			)
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andReturn().getResponse().getContentAsString();
		NotesDto response = mapper.readValue(responseBody, NotesDto.class);
		
		Assertions.assertNotNull(response);
		Assertions.assertEquals(0, response.getCurrentPage());
		Assertions.assertEquals(1, response.getTotalItems());
		Assertions.assertNotNull(response.getData());
		Assertions.assertEquals(1, response.getData().size());
		Assertions.assertEquals(1l, response.getData().get(0).getId());
	}

	private Note createExistingNote() {
		return new Note(1l, "admin", "Shopping list", "eggs, tomatoes, onions", "CREATED", new Date(), new Date());
	}

}
