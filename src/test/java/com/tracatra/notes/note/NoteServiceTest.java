package com.tracatra.notes.note;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tracatra.notes.dao.NoteDao;

@ExtendWith(MockitoExtension.class)
class NoteServiceTest {

	@InjectMocks
	private NoteService noteService;

	@Mock
	private NoteDao noteDao;

	@Test
	@DisplayName("Checking dao failure handling when adding a note")
	public void whenAddingNoteAndDaoFails_thenNoteExceptionIsThrown() {
		Note note = createExistingNote();
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.FALSE);
		Mockito.when(noteDao.create(note)).thenThrow(RuntimeException.class);
		
		Assertions.assertThrows(NoteException.class, ()->noteService.addNote(note));
	}

	@Test
	@DisplayName("Checking dao failure handling when updating a note")
	public void whenUpdatingNoteAndDaoFails_thenNoteExceptionIsThrown() {
		Note note = createExistingNote();
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.noteBelongsToUser(note.getUser(), note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.update(note)).thenThrow(RuntimeException.class);
		
		Assertions.assertThrows(NoteException.class, ()->noteService.updateNote(note));
	}

	@Test
	@DisplayName("Checking dao failure handling when deleting a note")
	public void whenDeletingNoteAndDaoFails_thenNoteExceptionIsThrown() {
		Note note = createExistingNote();
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.noteBelongsToUser(note.getUser(), note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.deleteById(note.getId())).thenThrow(RuntimeException.class);
		
		Assertions.assertThrows(NoteException.class, ()->noteService.deleteNote(note.getUser(), note.getId()));
	}

	@Test
	@DisplayName("Checking dao failure handling when listing notes")
	public void whenListingUserNotesAndDaoFails_thenNoteExceptionIsThrown() {
		Mockito.when(noteDao.getUserNotes("admin")).thenThrow(RuntimeException.class);
		
		Assertions.assertThrows(NoteException.class, ()->noteService.listUserNotes("admin"));
	}

	@Test
	@DisplayName("Checking dao failure handling when searchingNotes")
	public void whenSearchingNotesAndDaoFails_thenNoteExceptionIsThrown() {
		NoteQuery query = createNoteQuery();
		Mockito.when(noteDao.findNotes(query)).thenThrow(RuntimeException.class);
		
		Assertions.assertThrows(NoteException.class, ()->noteService.findNotes(query));
	}

	@Test
	@DisplayName("Checking note creation")
	public void givenNoNotes_whenCreateNote_thenNoteCreated() {
		Note newNote = createNewNote();
		Note createdNote = createExistingNote();
		Mockito.when(noteDao.existsById(null)).thenReturn(Boolean.FALSE);
		Mockito.when(noteDao.create(newNote)).thenReturn(createdNote);
		
		Assertions.assertEquals(createdNote.getId(), noteService.addNote(newNote).getId());
	}

	@Test
	@DisplayName("Checking note creation failsafe for existing notes")
	public void givenANote_whenCreateThatNote_thenExceptionThrown() {
		Note createdNote = createExistingNote();
		Mockito.when(noteDao.existsById(createdNote.getId())).thenReturn(Boolean.TRUE);
		
		Assertions.assertThrows(EntityExistsException.class, ()->noteService.addNote(createdNote));
		
		Mockito.verify(noteDao, Mockito.never()).create(createdNote);
	}

	@Test
	@DisplayName("Checking note update")
	public void givenANote_whenUpdateThatNote_thenNoteUpdated() {
		Note note = createExistingNote();
		Note updateRequest = createExistingNote("eggs, tomatoes, onions, milk");
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.noteBelongsToUser(note.getUser(), note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.update(updateRequest)).thenReturn(updateRequest);
		
		Assertions.assertEquals("eggs, tomatoes, onions, milk", noteService.updateNote(note).getText());
	}

	@Test
	@DisplayName("Checking note update failsafe for nonexisting notes")
	public void givenNoNotes_whenUpdateNote_thenExceptionThrown() {
		Note note = createExistingNote();
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.FALSE);
		
		Assertions.assertThrows(EntityNotFoundException.class, ()->noteService.updateNote(note));
		
		Mockito.verify(noteDao, Mockito.never()).noteBelongsToUser(note.getUser(), note.getId());
		Mockito.verify(noteDao, Mockito.never()).update(note);
	}

	@Test
	@DisplayName("Checking note update failsafe for not owned notes")
	public void givenANote_whenUpdateNoteFromOtherUser_thenExceptionThrown() {
		Note note = createExistingNote();
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.noteBelongsToUser(note.getUser(), note.getId())).thenReturn(Boolean.FALSE);
		
		Assertions.assertThrows(EntityNotFoundException.class, ()->noteService.updateNote(note));
		
		Mockito.verify(noteDao, Mockito.never()).update(note);
	}

	@Test
	@DisplayName("Checking note deletion")
	public void givenANote_whenDeleteThatNote_thenNoteDeleted() {
		Note note = createExistingNote();
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.noteBelongsToUser(note.getUser(), note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.deleteById(note.getId())).thenReturn(note);
		
		Assertions.assertEquals(note, noteService.deleteNote(note.getUser(), note.getId()));
	}

	@Test
	@DisplayName("Checking note deletion failsafe for nonexisting notes")
	public void givenNoNote_whenDeleteThatNote_thenExceptionThrown() {
		Note note = createExistingNote();
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.FALSE);
		
		Assertions.assertThrows(EntityNotFoundException.class, ()->noteService.deleteNote(note.getUser(), note.getId()));
		
		Mockito.verify(noteDao, Mockito.never()).noteBelongsToUser(note.getUser(), note.getId());
		Mockito.verify(noteDao, Mockito.never()).deleteById(note.getId());
	}

	@Test
	@DisplayName("Checking note deletion failsafe for not owned notes")
	public void givenANote_whenDeleteNoteFromOtherUser_thenNoteDeleted() {
		Note note = createExistingNote();
		Mockito.when(noteDao.existsById(note.getId())).thenReturn(Boolean.TRUE);
		Mockito.when(noteDao.noteBelongsToUser(note.getUser(), note.getId())).thenReturn(Boolean.FALSE);
		
		Assertions.assertThrows(EntityNotFoundException.class, ()->noteService.deleteNote(note.getUser(), note.getId()));
		
		Mockito.verify(noteDao, Mockito.never()).deleteById(note.getId());
	}

	@Test
	@DisplayName("Checking note list")
	public void givenANote_whenListUserNotes_thenNoteFound() {
		List<Note> notes = List.of(createExistingNote());
		String user = "admin";
		Mockito.when(noteDao.getUserNotes(user)).thenReturn(notes);
		
		List<Note> listedNotes = noteService.listUserNotes(user);
		
		Assertions.assertEquals(notes.get(0), listedNotes.get(0));
	}

	@Test
	@DisplayName("Checking note search")
	public void givenANote_whenSearchNotes_thenNoteFound() {
		NoteQuery query = createNoteQuery();
		Notes noteList = createNoteList();
		Mockito.when(noteDao.findNotes(query)).thenReturn(noteList);
		
		Notes found = noteService.findNotes(query);
		
		Assertions.assertTrue(found.getData().get(0).getTitle().contains(query.getSample().getTitle()));
	}

	private Note createNewNote() {
		return new Note(null, "admin", "Shopping list", "eggs, tomatoes, onions", null, null, null);
	}

	private Note createExistingNote() {
		return new Note(1l, "admin", "Shopping list", "eggs, tomatoes, onions", "CREATED", new Date(), new Date());
	}

	private Note createExistingNote(String text) {
		return new Note(1l, "admin", "Shopping list", text, "CREATED", new Date(), new Date());
	}

	private NoteQuery createNoteQuery() {
		Note sample = new Note(null, null, "list", null, null, null, null);
		return new NoteQuery(sample, null, null, null, null);
	}

	private Notes createNoteList() {
		return new Notes(List.of(createExistingNote()), 0, 1);
	}
}
