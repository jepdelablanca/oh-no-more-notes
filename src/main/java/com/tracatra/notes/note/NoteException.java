package com.tracatra.notes.note;

import java.util.Arrays;
import java.util.List;

/**
 * Exception used by the note service as 
 * wrapper for all related errors.
 *
 */
public class NoteException extends RuntimeException {

	private static final long serialVersionUID = 7717634557450843056L;

	private final String key;
	private List<String> arguments;

	public NoteException(String message, String key) {
		this(message, key, null, (String[]) null);
	}

	public NoteException(String message, String key, String... arguments) {
		this(message, key, null, arguments);
	}

	public NoteException(String message, String key, Throwable cause) {
		this(message, key, cause, (String[]) null);
		if (cause != null) {
			initCause(cause);
		}
	}

	public NoteException(String message, String key, Throwable cause, String... arguments) {
		super(message);
		this.key = key;
		if (cause != null) {
			initCause(cause);
		}
		this.arguments = Arrays.asList(arguments);
	}

	public String getKey() {
		return key;
	}

	public List<String> getArguments() {
		return arguments;
	}
}
