package com.tracatra.notes.note;

/**
 * A note search query. Includes search terms, 
 * and sorting and pagination attributes.
 *
 */
public class NoteQuery {

	public static final Integer DIRECTION_ASCENDING = 1;
	public static final Integer DIRECTION_DESCENDING = -1;

	private static final Integer DEFAULT_FROM = 0;
	private static final Integer DEFAULT_SIZE = 10;
	private static final Integer DEFAULT_SORT_DIRECTION = DIRECTION_ASCENDING;

	private Note sample;

	private Integer from;

	private Integer pageSize;

	private String sortBy;

	private Integer sortDirection;

	public NoteQuery(Note sample, Integer from, Integer pageSize, String sortBy, Integer sortDirection) {
		this.sample = sample != null ? sample : new Note();
		this.from = from != null ? from : DEFAULT_FROM;
		this.pageSize = pageSize != null ? pageSize : DEFAULT_SIZE;
		this.sortBy = sortBy;
		this.sortDirection = sortDirection != null ? sortDirection : DEFAULT_SORT_DIRECTION;
	}

	public Note getSample() {
		return sample;
	}

	public Integer getFrom() {
		return from;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public String getSortBy() {
		return sortBy;
	}

	public Integer getSortDirection() {
		return sortDirection;
	}

	@Override
	public String toString() {
		return "NoteQuery [sample=" + sample + ", from=" + from + ", pageSize=" + pageSize + ", sortBy=" + sortBy
				+ ", sortDirection=" + sortDirection + "]";
	}
}
