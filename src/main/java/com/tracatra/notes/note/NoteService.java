package com.tracatra.notes.note;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.tracatra.notes.dao.NoteDao;

/**
 * The notes service, providing the business logic of the application.
 *
 */
@Service
public class NoteService {

	private static final Logger LOG = LogManager.getLogger(NoteService.class);

	private NoteDao noteDao;

	public NoteService(NoteDao notesDao) {
		this.noteDao = notesDao;
	}

	public Note addNote(Note note) {
		if(noteDao.existsById(note.getId())) {
			throw new EntityExistsException("Note already exists");
		}
		try {
			Note added = noteDao.create(note);
			LOG.info("Added note. [note: {}]", added);
			return added;
		} catch (Throwable t) {
			throw new NoteException("Error adding note", "error.addNote", t, t == null ? "null" : t.getMessage());
		}
	}

	public Note updateNote(Note note) {
		if(!noteDao.existsById(note.getId()) || !noteDao.noteBelongsToUser(note.getUser(), note.getId())) {
			throw new EntityNotFoundException("Note does not exist");
		}
		try {
			Note updated = noteDao.update(note);
			LOG.info("Updated note. [note: {}]", updated);
			return updated;
		} catch (Throwable t) {
			throw new NoteException("Error updating note", "error.updateNote", t, t == null ? "null" : t.getMessage());
		}
	}

	public Note deleteNote(String username, Long noteId) {
		if (!noteDao.existsById(noteId) || !noteDao.noteBelongsToUser(username, noteId)) {
			throw new EntityNotFoundException("Note does not exist");
		}
		try {
			Note deleted = noteDao.deleteById(noteId);
			LOG.info("Deleted note. [note-id: {}, user: {}]", noteId, username);
			return deleted;
		} catch (Throwable t) {
			throw new NoteException("Error deleting note", "error.deleteNote", t, t == null ? "null" : t.getMessage());
		}
	}

	public List<Note> listUserNotes(String username) {
		try {
			return noteDao.getUserNotes(username);
		} catch (Throwable t) {
			throw new NoteException("Error listing notes", "error.listUserNotes", t, t == null ? "null" : t.getMessage());
		}
	}

	public Notes findNotes(NoteQuery query) {
		try {
			return noteDao.findNotes(query);
		} catch (Throwable t) {
			throw new NoteException("Error searching notes", "error.findNotes", t, t == null ? "null" : t.getMessage());
		}
	}
}
