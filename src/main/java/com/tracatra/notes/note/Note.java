package com.tracatra.notes.note;

import java.util.Date;

/**
 * An entity representing a note.
 *
 */
public class Note {

	private Long id;

	private String title;

	private String text;

	private String status;

	private String user;

	private Date creationDate;

	private Date lastModified;

	public Note() {}

	public Note(Long id, String user, String title, String text, String status, Date creationDate,
			Date lastModified) {
		this.id = id;
		this.title = title;
		this.text = text;
		this.status = status;
		this.user = user;
		this.creationDate = creationDate;
		this.lastModified = lastModified;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getText() {
		return text;
	}

	public String getStatus() {
		return status;
	}

	public String getUser() {
		return user;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Date getLastModified() {
		return lastModified;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Note other = (Note) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Note [id=" + id + ", title=" + title + ", text=" + text + ", status=" + status + ", user=" + user
				+ ", creationDate=" + creationDate + ", lastModified=" + lastModified + "]";
	}
}
