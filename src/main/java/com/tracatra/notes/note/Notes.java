package com.tracatra.notes.note;

import java.util.ArrayList;
import java.util.List;

/**
 * A paginated list of notes. Used in the service search operation.
 *
 */
public class Notes {

	private List<Note> data;

	private Integer currentPage;

	private Integer totalItems;

	public Notes(List<Note> data, Integer currentPage, Integer totalItems) {
		this.data = data != null ? data : new ArrayList<>();
		this.currentPage = currentPage;
		this.totalItems = totalItems;
	}

	public List<Note> getData() {
		return data;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public Integer getTotalItems() {
		return totalItems;
	}

	@Override
	public String toString() {
		return "NoteList [data=" + data + ", currentPage=" + currentPage + ", totalItems=" + totalItems + "]";
	}
}