package com.tracatra.notes.dao;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.exact;
import static org.springframework.data.domain.Sort.Order.asc;
import static org.springframework.data.domain.Sort.Order.desc;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tracatra.notes.note.Note;
import com.tracatra.notes.note.Notes;
import com.tracatra.notes.note.NoteQuery;

/**
 * The notes data access object.
 * Provides a normalized access to the note repository,
 * hiding the database specific aspects to the invoker.
 *
 */
@Repository
public class NoteDao {

	private static final ExampleMatcher NOTE_MATCHER = ExampleMatcher.matching()
			.withIgnoreNullValues()
			.withMatcher("title", contains().ignoreCase())
			.withMatcher("text", contains().ignoreCase())
			.withMatcher("user", exact());

	private NoteRepository noteRepository;

	public NoteDao(NoteRepository noteRepository) {
		this.noteRepository = noteRepository;
	}

	@Transactional(readOnly = true)
	public boolean existsById(Long id) {
		return id != null && noteRepository.existsById(id);
	}

	@Transactional(readOnly = true)
	public List<Note> getUserNotes(String user) {
		return noteRepository.getUserNotes(user).stream().map(note -> toEntity(note)).collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	public Optional<Note> getNote(Long id) {
		return noteRepository.find(id).map(note -> toEntity(note));
	}

	@Transactional(readOnly = true)
	public boolean noteBelongsToUser(String username, Long noteId) {
		return noteRepository.findUserNote(noteId, username).isPresent();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Note create(Note note) {
		return toEntity(noteRepository.save(toJpa(note, "CREATED")));
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Note update(Note note) {
		return toEntity(noteRepository.save(toJpa(note, "UPDATED")));
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Note deleteById(Long id) {
		NoteJpa note = noteRepository.findById(id).orElseThrow();
		note.setStatus("DELETED");
		return toEntity(noteRepository.save(note));
	}

	@Transactional(readOnly = true)
	public Notes findNotes(NoteQuery query) {
		Example<NoteJpa> example = Example.of(toJpa(query), NOTE_MATCHER);

		Sort sort = Sort.unsorted();
		if (query.getSortBy() != null) {
			boolean ascending = query.getSortDirection() == NoteQuery.DIRECTION_ASCENDING;
			sort = Sort.by(ascending ? asc(query.getSortBy()) : desc(query.getSortBy()));
		}
		int pageNumber = query.getFrom() % query.getPageSize();
		Pageable pageable = PageRequest.of(pageNumber, query.getPageSize(), sort);

		Iterable<NoteJpa> page = noteRepository.findAll(example, pageable);
		List<Note> notes = StreamSupport.stream(page.spliterator(), false).map(dbNote -> toEntity(dbNote))
				.collect(Collectors.toList());

		Long totalCount = noteRepository.count(example);

		return new Notes(notes, pageNumber, totalCount.intValue());
	}

	private Note toEntity(NoteJpa from) {
		return new Note(from.getId(), from.getUsername(), from.getTitle(), from.getText(), from.getStatus(), from.getCreationDate(), from.getLastModified());
	}

	private NoteJpa toJpa(Note from, String status) {
		return new NoteJpa(from.getId(), from.getUser(), from.getTitle(), from.getText(), status, null, null);
	}

	private NoteJpa toJpa(NoteQuery query) {
		return new NoteJpa(null, query.getSample().getUser(), query.getSample().getTitle(), query.getSample().getText(), null, null, null);
	}
}
