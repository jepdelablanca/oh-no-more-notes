package com.tracatra.notes.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * The notes repository.
 * Provides inherited CRUD operations on the note repository,
 * and convenience methods for easy access.
 *
 */
@Repository
@Transactional(readOnly = true)
interface NoteRepository extends CrudRepository<NoteJpa, Long>, QueryByExampleExecutor<NoteJpa> {

	public default Optional<NoteJpa> find(Long id) {
		return findByIdAndStatusIsNot(id, "DELETED");
	}

	public default Optional<NoteJpa> findUserNote(Long id, String username) {
		return findByIdAndUsernameAndStatusIsNot(id, username, "DELETED");
	}

	public default List<NoteJpa> getUserNotes(String user) {
		return findByUsernameAndStatusIsNotOrderByCreationDateDesc(user, "DELETED");
	}

	Optional<NoteJpa> findByIdAndStatusIsNot(Long id, String status);

	Optional<NoteJpa> findByIdAndUsernameAndStatusIsNot(Long id, String username, String status);

	List<NoteJpa> findByUsernameAndStatusIsNotOrderByCreationDateDesc(String user, String status);

}