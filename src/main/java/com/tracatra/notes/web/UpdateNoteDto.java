package com.tracatra.notes.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A data transfer object used for the update of notes.
 * Provides descriptions for the swagger interface.
 *
 */
@ApiModel(
	value = "Update note request",
	description = "The request for a note update"
)
@JsonPropertyOrder({ "id", "title", "text" })
class UpdateNoteDto {

	@NotNull
	@ApiModelProperty(
		position = 1,
		value = "The id of the note. This field is mandatory",
		allowEmptyValue = false,
		required = true,
		example = "1"
	)
	private Long id;

	@NotNull
	@Size(max = 255)
	@ApiModelProperty(
		position = 2,
		value = "The title of the note",
		allowEmptyValue = false,
		example = "Shopping list"
	)
	private String title;

	@Size(max = 10240)
	@ApiModelProperty(
		position = 3,
		value = "The text of the note",
		example = "Potatoes, Milk"
	)
	private String text;

	public static UpdateNoteDto of(Long id, String title, String text) {
		UpdateNoteDto result = new UpdateNoteDto();
		result.id = id;
		result.title = title;
		result.text = text;
		return result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}