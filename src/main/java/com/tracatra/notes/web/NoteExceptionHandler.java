package com.tracatra.notes.web;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.tracatra.notes.note.NoteException;

/**
 * Handles the application errors, providing custom 
 * responses to specific error types.
 *
 */
@ControllerAdvice
class NoteExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOG = LogManager.getLogger(NoteExceptionHandler.class);

	private Translator translator;

	public NoteExceptionHandler(Translator translator) {
		this.translator = translator;
	}

	@ExceptionHandler(value = EntityNotFoundException.class)
	protected ResponseEntity<Object> handleEntityNotFoundException(RuntimeException exception, WebRequest request) {
		LOG.warn("Caught EntityNotFoundException: {}", (exception == null ? "null" : exception.getMessage()), exception);
		ControllerResponseDto response = ControllerResponseDto.error("error.entityNotFound");
		return handleExceptionInternal(exception,
				translator.translate(response), new HttpHeaders(), 
				HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(value = EntityExistsException.class)
	protected ResponseEntity<Object> handleEntityExistsException(RuntimeException exception, WebRequest request) {
		LOG.warn("Caught EntityExistsException: {}", (exception == null ? "null" : exception.getMessage()), exception);
		ControllerResponseDto response = ControllerResponseDto.error("error.entityExists");
		return handleExceptionInternal(exception,
				translator.translate(response), new HttpHeaders(), 
				HttpStatus.CONFLICT, request);
	}

	@ExceptionHandler(value = NoteException.class)
	protected ResponseEntity<Object> handleNotesException(RuntimeException exception, WebRequest request) {
		LOG.warn("Caught NoteException: {}", (exception == null ? "null" : exception.getMessage()), exception);
		return handleExceptionInternal(exception,
				translator.translate(ControllerResponseDto.error((NoteException) exception)), 
				new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	@ExceptionHandler(value = RuntimeException.class)
	protected ResponseEntity<Object> handleRuntimeException(RuntimeException exception, WebRequest request) {
		LOG.error("Caught RuntimeException: {}", (exception == null ? "null" : exception.getMessage()), exception);
		ControllerResponseDto response = ControllerResponseDto.error("error.server", "message",
				exception == null ? "null" : exception.getMessage());
		return handleExceptionInternal(exception, 
				translator.translate(response), new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
}
