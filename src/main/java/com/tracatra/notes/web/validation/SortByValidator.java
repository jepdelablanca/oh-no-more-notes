package com.tracatra.notes.web.validation;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Bean validator for the {@code NotesQuery.sortBy} field.
 * The only accepted values are {@code null}, {@code title} and {@code text}.
 *
 */
class SortByValidator implements ConstraintValidator<SortByConstraint, String> {

	private static final List<String> VALID_VALUES = Arrays.asList("title", "text");

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value == null || VALID_VALUES.contains(value);
	}
}
