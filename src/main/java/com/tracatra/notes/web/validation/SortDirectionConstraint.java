package com.tracatra.notes.web.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Bean validation constraint for the {@code NotesQuery.sortDirection} field.
 *
 */
@Documented
@Constraint(validatedBy = SortDirectionValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface SortDirectionConstraint {

	String message() default "Invalid sort direction, valid values are -1 and 1";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}