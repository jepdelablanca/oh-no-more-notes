package com.tracatra.notes.web.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Bean validation constraint for the {@code NotesQuery.sortBy} field.
 *
 */
@Documented
@Constraint(validatedBy = SortByValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface SortByConstraint {

	String message() default "Invalid sort field, valid values are title and text";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
