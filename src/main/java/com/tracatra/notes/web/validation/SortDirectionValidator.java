package com.tracatra.notes.web.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Bean validator for the {@code NotesQuery.sortDirection} field.
 * The only accepted values are {@code null}, {@code -1} and {@code 1}.
 *
 */
class SortDirectionValidator implements ConstraintValidator<SortDirectionConstraint, Integer> {

	@Override
	public void initialize(SortDirectionConstraint sortDirection) {
	}

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		return value == null || value == -1 || value == 1;
	}

}
