package com.tracatra.notes.web;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tracatra.notes.note.NoteException;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A response returned by the controller.
 * Represents a success, warning or error message.
 * Provides descriptions for the swagger interface.
 *
 */
@ApiModel(
	value = "Controller response",
	description = "A response provided by the controller"
)
@JsonPropertyOrder({ "type", "message", "key", "arguments" })
class ControllerResponseDto {

	@ApiModelProperty(
		position = 1,
		value = "The type of response.",
		allowableValues = "success, warning, error",
		example = "success"
	)
	private String type;

	@ApiModelProperty(
		position = 2,
		value = "The response internationalization key",
		example = "success.addNote"
	)
	private String key;

	@ApiModelProperty(
		position = 3,
		value = "A list of arguments for the internationalization key",
		example = "[\"admin\"]"
	)
	private List<String> arguments;

	@ApiModelProperty(
		position = 4,
		value = "The translated response message",
		example = "Se ha creado la nota para el usuario admin"
	)
	private String message;

	@ApiModelProperty(
		position=5,
		value="The affected note",
		example = "{id: 1, title: \"Shopping list\", text: \"Potatoes, Milk\"}"
	)
	private NoteResponseDto entity;

	public ControllerResponseDto(String type, String key, NoteResponseDto entity, String... arguments) {
		this.type = type;
		this.key = key;
		this.entity = entity;
		this.arguments = Arrays.asList(arguments != null ? arguments : new String[] {});
	}

	public static ControllerResponseDto success(String key, NoteResponseDto entity, String... arguments) {
		return new ControllerResponseDto("success", key, entity, arguments);
	}

	public static ControllerResponseDto warning(String key, String... arguments) {
		return new ControllerResponseDto("warning", key, null, arguments);
	}

	public static ControllerResponseDto error(String key, String... arguments) {
		return new ControllerResponseDto("error", key, null, arguments);
	}

	public static ControllerResponseDto error(NoteException exception) {
		String[] args = exception.getArguments() == null ? null : exception.getArguments().toArray(String[]::new);
		return new ControllerResponseDto("error", exception.getKey(), null, args);
	}

	public String getType() {
		return type;
	}

	public String getKey() {
		return key;
	}

	public List<String> getArguments() {
		return arguments;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public NoteResponseDto getEntity() {
		return entity;
	}
}
