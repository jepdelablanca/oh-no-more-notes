package com.tracatra.notes.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tracatra.notes.note.Note;
import com.tracatra.notes.note.NoteQuery;
import com.tracatra.notes.note.NoteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The notes REST controller.
 * Provides access to the notes service,
 * validating the requests and providing a concrete
 * model for each operation.
 * Provides descriptions for the swagger interface.
 * Also translates the response messages.
 *
 */
@Validated
@RestController
@RequestMapping("/note")
@Api(value = "note", consumes = "application/json", produces = "application/json")
class NoteController {

	private NoteService notesService;

	private Translator translator;

	public NoteController(NoteService noteService, Translator translator) {
		this.notesService = noteService;
		this.translator = translator;
	}

	@PostMapping
	@Secured("ROLE_USER")
	@ApiOperation(value = "Adds a note", notes = "The note will be assigned to the current user")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ControllerResponseDto addNote(@Valid @ApiParam(value = "The note to create", required = true) @RequestBody CreateNoteDto createNote) {
		String username = getCurrentUser();
		Note added = notesService.addNote(toEntity(createNote, username));
		return translator.translate(ControllerResponseDto.success("success.addNote", toDto(added), username));
	}

	@PutMapping
	@Secured("ROLE_USER")
	@ApiOperation(value = "Updates a note", notes = "The note must belong to the current user")
	public ControllerResponseDto updateNote(@Valid @ApiParam(value = "The note to update", required = true)@RequestBody UpdateNoteDto updateNote) {
		String username = getCurrentUser();
		Note updated = notesService.updateNote(toEntity(updateNote, username));
		return translator
				.translate(ControllerResponseDto.success("success.updateNote",toDto(updated), updateNote.getId().toString(), username));
	}

	@Secured("ROLE_USER")
	@DeleteMapping("/{noteId}")
	@ApiOperation(value = "Deletes a note", notes = "The note must belong to the current user. This method performs a logical erase")
	public ControllerResponseDto deleteNote(
			@NotNull @Min(0) @ApiParam(value = "The note id. This field is mandatory", required = true, example = "1", allowableValues = "range[0, infinity]") @PathVariable Long noteId) {
		String username = getCurrentUser();
		Note deleted = notesService.deleteNote(username, noteId);
		return translator.translate(ControllerResponseDto.success("success.deleteNote", toDto(deleted), noteId.toString(), username));
	}

	@GetMapping
	@Secured("ROLE_USER")
	@ApiOperation(value = "Lists your notes")
	public List<NoteResponseDto> listUserNotes() {
		return notesService.listUserNotes(getCurrentUser()).stream().map(note -> new NoteResponseDto(note))
				.collect(Collectors.toList());
	}

	@Secured("ROLE_ADMIN")
	@PutMapping("/query")
	@ApiOperation(value = "Searches for notes", notes = "You need to be administrator to perform this operation, as it will include deleted notes and notes from other users")
	public NotesDto findNotes(@Valid @ApiParam(value = "The notes search query", required = true)@RequestBody QueryDto query) {
		return new NotesDto(notesService.findNotes(toEntity(query)));
	}

	private String getCurrentUser() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}

	private Note toEntity(CreateNoteDto createNote, String username) {
		return new Note(null, username, createNote.getTitle(), createNote.getText(), null, null, null);
	}

	private Note toEntity(UpdateNoteDto updateNote, String username) {
		return new Note(updateNote.getId(),username,  updateNote.getTitle(), updateNote.getText(), null, null, null);
	}

	private NoteQuery toEntity(QueryDto query) {
		Note sample = new Note(null, query.getUsername(), query.getTitle(), query.getText(), null, null, null);
		return new NoteQuery(sample, query.getFrom(), query.getPageSize(), query.getSortBy(), query.getSortDirection());
	}

	private NoteResponseDto toDto(Note note) {
		return new NoteResponseDto(note);
	}
}