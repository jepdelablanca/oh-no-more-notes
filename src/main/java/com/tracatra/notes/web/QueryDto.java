package com.tracatra.notes.web;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tracatra.notes.web.validation.SortByConstraint;
import com.tracatra.notes.web.validation.SortDirectionConstraint;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data transfer object representing a query for notes.
 * Provides descriptions for the swagger interface.
 *
 */
@ApiModel(
	value = "Notes query request",
	description = "Arguments for a note search operation"
)
@JsonPropertyOrder({ "username", "title", "text", "from", "pageSize", "sortBy", "sortDirection" })
class QueryDto {

	@Size(max = 50)
	@ApiModelProperty(
		position = 1,
		value = "User owner of the notes. The operation will search any notes that belong to this user",
		example = "admin"
	)
	private String username;

	@Size(max = 255)
	@ApiModelProperty(
		position = 2,
		value = "Title search term. The operation will search any notes that contain this term in their title",
		example = "list"
	)
	private String title;

	@Size(max = 10240)
	@ApiModelProperty(
		position = 3,
		value = "Text search term. The operation will search any notes that contain this term in their text",
		example = "Milk"
	)
	private String text;

	@Min(0)
	@NotNull
	@ApiModelProperty(
		position = 4,
		value = "The starting count for the pagination. The result will contain the notes found starting from this position",
		allowableValues = "range[0, infinity]",
		example = "0"
	)
	private Integer from;

	@Min(10)
	@NotNull
	@ApiModelProperty(
		position = 5,
		value = "The size of the resulting list. The result will contain up to the specified number of elements",
		allowableValues = "range[10, infinity]",
		example = "10"
	)
	private Integer pageSize;

	@SortByConstraint
	@ApiModelProperty(
		position = 6,
		value = "The field used to sort the results",
		allowableValues = "title, text",
		example = "title"
	)
	private String sortBy;

	@SortDirectionConstraint
	@ApiModelProperty(
		position = 7,
		value = "The direction used to sort the results",
		allowableValues = "-1, 1",
		example = "1"
	)
	private Integer sortDirection;

	public static QueryDto of(String username, String title, String text, Integer from, Integer pageSize, String sortBy, Integer sortDirection) {
		QueryDto result = new QueryDto();
		result.username = username;
		result.title = title;
		result.text = text;
		result.from = from;
		result.pageSize = pageSize;
		result.sortBy = sortBy;
		result.sortDirection = sortDirection;
		return result;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getFrom() {
		return from;
	}

	public void setFrom(Integer from) {
		this.from = from;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public Integer getSortDirection() {
		return sortDirection;
	}

	public void setSortDirection(Integer sortDirection) {
		this.sortDirection = sortDirection;
	}
}
