package com.tracatra.notes.web;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tracatra.notes.note.Note;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A data transfer object used to display notes.
 * Provides descriptions for the swagger interface.
 *
 */
@ApiModel(
	value = "Note response",
	description = "A user note"
)
@JsonPropertyOrder({ "id", "title", "text", "status", "user", "creationDate", "lastModified" })
class NoteResponseDto {

	@ApiModelProperty(
		position = 1,
		value = "The note id",
		example = "1"
	)
	private Long id;
	
	@ApiModelProperty(
		position = 2,
		value = "The title of the note",
		example = "Shopping list"
	)
	private String title;

	@ApiModelProperty(
		position = 3,
		value = "The text of the note",
		example = "Potatoes, Milk"
	)
	private String text;

	@ApiModelProperty(
		position = 4,
		value = "The note status",
		example = "CREATED",
		allowableValues = "CREATED, UPDATED, DELETED"
	)
	private String status;

	@ApiModelProperty(
		position = 5,
		value = "The note owner",
		example = "admin"
	)
	private String user;

	@ApiModelProperty(
		position = 6,
		value = "The note creation date, specified in milliseconds since epoch time",
		example = "1635356561197"
	)
	private Date creationDate;

	@ApiModelProperty(
		position = 7,
		value = "The note last update date, specified in milliseconds since epoch time",
		example = "1635359306299"
	)
	private Date lastModified;

	public NoteResponseDto() {}

	public NoteResponseDto(Note from) {
		this.id = from.getId();
		this.title = from.getTitle();
		this.text = from.getText();
		this.status = from.getStatus();
		this.user = from.getUser();
		this.creationDate = from.getCreationDate();
		this.lastModified = from.getLastModified();
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getText() {
		return text;
	}

	public String getStatus() {
		return status;
	}

	public String getUser() {
		return user;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Date getLastModified() {
		return lastModified;
	}

}
