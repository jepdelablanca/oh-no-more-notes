package com.tracatra.notes.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A data transfer object used for the creation of notes.
 * Provides descriptions for the swagger interface.
 *
 */
@ApiModel(
	value = "New note request",
	description = "A note that will be created"
)
@JsonPropertyOrder({ "title", "text" })
class CreateNoteDto {

	@NotNull
	@Size(max = 255)
	@ApiModelProperty(
		value = "The title of the note",
		allowEmptyValue = false,
		example = "Shopping list"
	)
	private String title;

	@Size(max = 10240)
	@ApiModelProperty(
		value = "The text of the note",
		example = "Potatoes, Milk"
	)
	private String text;

	public static CreateNoteDto of(String title, String text) {
		CreateNoteDto result = new CreateNoteDto();
		result.title = title;
		result.text = text;
		return result;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}