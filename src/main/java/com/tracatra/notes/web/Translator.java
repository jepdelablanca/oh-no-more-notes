package com.tracatra.notes.web;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

/**
 * Translation service.
 * Allows the translation of {@code ControllerResponse} entities.
 *
 */
@Service
class Translator {

	private MessageSource messageSource;

	public Translator(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public ControllerResponseDto translate(ControllerResponseDto response) {
		response.setMessage(messageSource.getMessage(
				response.getKey(),
				response.getArguments().toArray(),
				LocaleContextHolder.getLocale()
		));
		return response;
	}
}
