package com.tracatra.notes.web;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tracatra.notes.note.Notes;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data transfer object representing a paginated list of notes.
 * Provides descriptions for the swagger interface.
 *
 */
@ApiModel(
	value = "Paginated notes list response",
	description = "A paginated list of notes"
)
@JsonPropertyOrder({ "currentPage", "totalItems", "data" })
class NotesDto {

	@ApiModelProperty(
		position = 1,
		value = "The list of notes for the current page"
	)
	private List<NoteResponseDto> data;

	@ApiModelProperty(
		position = 2,
		value = "The current page index",
		example = "1"
	)
	private Integer currentPage;

	@ApiModelProperty(
		position = 3,
		value = "The total of items matching the search",
		example = "12"
	)
	private Integer totalItems;

	public NotesDto() {}

	public NotesDto(Notes from) {
		this.data = from.getData().stream().map(note->new NoteResponseDto(note)).collect(Collectors.toList());
		this.currentPage = from.getCurrentPage();
		this.totalItems = from.getTotalItems();
	}

	public List<NoteResponseDto> getData() {
		return data;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public Integer getTotalItems() {
		return totalItems;
	}
}