package com.tracatra.notes.config;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.springframework.web.servlet.LocaleResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

/**
 * Internationalization configuration component.
 * Sets up a locale resolver and a resource bundle message source.
 *
 */
@Configuration
class InternationalizationConfig {

	private static final Locale DEFAULT_LOCALE = new Locale("es");
	private static final List<Locale> AVAILABLE_LOCALES = Arrays.asList(DEFAULT_LOCALE);
	private static final String RESOURCES_BASENAME = "i18n/messages";

	@Bean
	public LocaleResolver localeResolver() {
		AcceptHeaderLocaleResolver resolver = new AcceptHeaderLocaleResolver();
		resolver.setDefaultLocale(DEFAULT_LOCALE);
		resolver.setSupportedLocales(AVAILABLE_LOCALES);
		return (LocaleResolver) resolver;
	}

	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasenames(RESOURCES_BASENAME);
		return source;
	}
}