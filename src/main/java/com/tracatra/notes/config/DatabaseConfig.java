package com.tracatra.notes.config;

import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Database configuration component.
 * Enables JPA entity auditing.
 * Enables transaction management.
 *
 */
@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
@EnableTransactionManagement
class DatabaseConfig {

	/**
	 * Provides a component that links the auditor with the current security context user.
	 */
	@Bean
	public AuditorAware<String> auditorProvider() {
		return new AuditorAware<String>() {
			
			@Override
			public Optional<String> getCurrentAuditor() {
				return Optional.of(SecurityContextHolder.getContext().getAuthentication()).map(auth -> auth.getName());
			}
		};
	}
}
