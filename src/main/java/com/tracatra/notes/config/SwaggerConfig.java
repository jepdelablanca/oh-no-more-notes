package com.tracatra.notes.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Swagger configuration component.
 * Enables the swagger web user interface for the 
 * spring boot profiles 'dev' and default.
 * Excludes the exception handler from the
 * listed interfaces.
 *
 */
@Configuration
class SwaggerConfig {

	@Value("${app.version}")
	private String appVersion;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
			.apiInfo(new ApiInfo(
				"Notes API",
				"Easily manage notes",
				appVersion,
				"",
				new Contact(
					"José Enrique Pérez de la Blanca",
					"",
					"jepdelablanca@gmail.com"
				), 
				"Apache 2.0",
				"http://www.apache.org/licenses/LICENSE-2.0",
				Collections.emptyList())
			)
			.select()
			.apis(RequestHandlerSelectors.any())
			.paths(PathSelectors.ant("/error").negate())
			.build();
	}
}
