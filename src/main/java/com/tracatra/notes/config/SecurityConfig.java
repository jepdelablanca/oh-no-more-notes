package com.tracatra.notes.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

/**
 * Security configuration component.
 * Enables web security and global method security.
 * Enforces a secure https channel, enforces HTTP basic 
 * auth and disables cross origin requests.
 * Provides an administrator user to access the application.
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class SecurityConfig {

	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		return http
				.requiresChannel(channel -> channel.anyRequest().requiresSecure())
				.authorizeRequests().anyRequest().authenticated().and().httpBasic()
				.and().csrf().disable()
				.build();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public UserDetailsService users(PasswordEncoder encoder) {
		UserDetails user = User.builder().username("user")
				.password(encoder.encode("user")).roles("USER").build();
		UserDetails admin = User.builder().username("admin")
				.password(encoder.encode("admin")).roles("USER", "ADMIN").build();
		return new InMemoryUserDetailsManager(user, admin);
	}
}