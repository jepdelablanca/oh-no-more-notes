# NOTES

This application provides a REST backend API to manage user tasks.

The API offers CRUD operations over tasks, plus a search feature for administrators.

## Usage

In order to build and run the application, execute the following command:

- bash:

```bash
./mvnw clean install spring-boot:run
```

- cmd:

```cmd
mvnw.cmd clean install spring-boot:run
```

## Swagger interface

The API is documented and can be tested via the Swagger user interface, at <https://localhost:8443/swagger-ui/>

The interface is secured with http basic auth. Please login using the following credentials:

```
username: user
password: user
```

The Swagger interface will automatically provide these same credentials when invoking the API.

There is a special case: the `search` operation requires administrator privileges. In order to successfully invoke this operation, you will need to access using admin credentials:

```
username: admin
password: admin
```

## Run profiles

There is a `dev` profile and a `prod` profile.

* When running with the default profile (neither `dev` nor `prod`), the `tasks` database is selected. This database is restored every time the application is built. This dafult profile acts as a `demo` mode, and the Swagger UI is enabled.

* When running the application with the `dev` profile, additional logs are enabled to show SQL queries and hibernate transactions. Also, the permanent `tasks-dev` database is used. In this `development` mode the Swagger UI is enabled as well.

* When running with the `prod` profile, an external `application.properties` should be provided to override the database connection parameters, otherwise the default `tasks` database will be used. When running in this `production` mode, the Swagger UI is disabled.

Switch profiles using the Spring Boot commandline argument:

- bash:

```
./mvnw spring-boot:run -Dspring-boot.run.profiles=dev
./mvnw spring-boot:run -Dspring-boot.run.profiles=prod
```

- cmd:

```
mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=dev
mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=prod
```

## Tests

There are unit tests for the service and rest controllers, with a 100% coverage.

There are also integration tests that cover the happy paths provided by the API.

## Code coverage

The `jacoco-maven-plugin` creates a code coverage report after tests are passed, during the `prepare-package` phase.

The report will be available as `html` and `csv` after compiling at:

```
target/site/jacoco/index.html
target/site/jacoco/jacoco.csv
```

## Technologies

For the realization of this project, the following technologies were used:

* Java 11
* Spring Boot 2.5.6
    * Spring Web
    * Spring Data JPA
    * Spring Security
    * Spring Validation
* H2 Database
* Swagger UI
* Jacoco

The `pom.xml` was created with `spring initializr` and is described in the following address: [Notes 2 Spring initializr][initializr]


[initializr]: https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.5.6&packaging=jar&jvmVersion=11&groupId=com.tracatra&artifactId=oh_no_more_notes&name=Notes%202&description=Notes%20application&packageName=com.tracatra.notes&dependencies=web,data-jpa,security,h2,validation
